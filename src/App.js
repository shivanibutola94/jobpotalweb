import logo from './logo.svg';
// import './App.css';
// import Demo from './components/Demo';
// import JobBrief from './components/JobBrief';
import JobBriefList from './components/JobBriefList';


function App() {
  
  return (
    <div className="App">
      {/* <Demo /> */}
      {/* <JobBrief /> */}
      <JobBriefList/>
    </div>
  );
}

export default App;
