import React from 'react'
import Secondimg from '../image/secondimg.jpg'

function JobBrief() {
    return (
        <div className="jobs" style={{maxWidth:"20vw"}}>
             <div>
        <h4>Full-stack developer</h4>
        <h5>Delhi</h5>
        <img
            src={Secondimg} alt="cardimg"
            className="job-post-image"
        />
        <p>
            Company looking for some crazy people, we are looking for challenge
            accepter who move with "I can do" attitude. Quick decision makers,
            living with the passion of work, never feeling or saying "I am tired"
        </p>
        <div className="salary">Salary: 30K - 40K</div>
        
        {/* <hr /> */}
        </div>
        </div>
    )
}

export default JobBrief
